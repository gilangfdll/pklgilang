<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $posts = [
            ['title'=>'Tips Cepat Menikah', 'content'=>'lorem ipsum'],
            ['title'=>'Haruskah Cepat Menikah', 'content'=>'lorem ipsum'],
            ['title'=>'Membangun Visi Misi Keluarga', 'content'=>'lorem ipsum']
        ];
        // Masukan Data ke database
        DB::table('posts')->insert($posts);
    }
}